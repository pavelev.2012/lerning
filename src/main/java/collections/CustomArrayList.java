package collections;

import org.example.ArrayUtils;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

import static org.example.ArrayUtils.copyArray;
import static org.example.ArrayUtils.fill;

public class CustomArrayList implements List<String> {
    private String[] massiv = new String[10];
    private int size = 0;


    @Override
    public boolean add(String string) {
        resizeIfNeed();
        int index = resolveNextIndex();
        massiv[index] = string;
        size++;
        return true;
    }

    private void resizeIfNeed() {
        if (arrayIsFull()) {
            resize(calculateNewCapacity(massiv.length));
        } else {
            // do nofing
        }
    }

    private int calculateNewCapacity(int length) {
        return length * 3 / 2 + 1;
    }

    private void resize(int targetSize) {
        String[] newMassiv = new String[targetSize];
        copyArray(massiv, newMassiv);
        massiv = newMassiv;
    }

    private boolean arrayIsFull() {
        return size >= massiv.length;
    }


    private int resolveNextIndex() {
        return size;
    }

    @Override
    public String get(int index) {
        validateIndexForGet(index, "Индекс говно");
        return massiv[index];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        if (size == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < size; i++) {
            if (Objects.equals(o, massiv[i])) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<String> iterator() {
        return new CustomIterator(this);
    }

    @Override
    public Object[] toArray() {
        String[] strings = new String[size];
        fill(strings, massiv);
        return strings;
    }


    @Override
    public <T> T[] toArray(T[] a) {
        T[] o = (T[]) Array.newInstance(a.getClass().getComponentType(), size);
        fill(o, massiv);
        return o;
    }

    @Override
    public boolean remove(Object o) {
        if (size == 0) {
            return false;
        }

        //  поиск
        int indexForRemove = getIndexForRemove(o);
        if (indexForRemove == -1) {
            return false;
        }
        //  удаление
        massiv[indexForRemove] = null;
        System.arraycopy(massiv, indexForRemove + 1, massiv, indexForRemove, size - indexForRemove - 1);
        massiv[size - 1] = null;
        size--;
        return true;
    }

    private int getIndexForRemove(Object o) {
        int indexForRemove = -1;
        for (int i = 0; i < size; i++) {
            if (Objects.equals(o, massiv[i])) {
                indexForRemove = i;
                break;
            } else {

            }
        }
        return indexForRemove;
    }

    @Override
    public String remove(int index) {
        String valueForRemove;
        valueForRemove = massiv[index];
        massiv[index] = null;
        System.arraycopy(massiv, index + 1, massiv, index, size - index - 1);
        massiv[size - 1] = null;
        size--;
        return valueForRemove;
    }

    @Override
    public int indexOf(Object o) {
        return getIndexForRemove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        if (c == null) {
            return false;
        } else {
            // do nofing
        }
        if (c.isEmpty()) {
            return true;
        }
        for (Object listok : c) {
            if (contains(listok)) {

            } else {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends String> c) {
        if (c == null || c.isEmpty()) {
            return false;
        } else {
            String[] arrayForAdd = c.toArray(new String[0]);
            int expectedSize = size + arrayForAdd.length;
            if ((massiv.length - size) < expectedSize) {
                resize(calculateNewCapacity(expectedSize));
            }
            System.arraycopy(arrayForAdd, 0, massiv, size, arrayForAdd.length);
            size = expectedSize;
            return true;
        }
    }

    @Override
    public boolean addAll(int index, Collection<? extends String> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            massiv[i] = null;
        }
        size = 0;

    }

    @Override
    public String set(int index, String element) {
        validateIndexForAdd(index, "Нельзя это сделать");
        String oldName;
        oldName = massiv[index];
        massiv[index] = element;
        return oldName;
    }

    @Override
    public void add(int index, String element) {
        validateIndexForAdd(index, "Индекс говно");
        resizeIfNeed();
        ArrayUtils.moveToRight(massiv, index, size);
        massiv[index] = null;
        massiv[index] = element;
        size++;
    }

    private void validateIndexForGet(int index, String string) {
        if (size <= index || index < 0) {
            throw new IllegalArgumentException(string);
        }
    }
    private void validateIndexForAdd(int index, String string) {
        if (size < index || index < 0) {
            throw new IllegalArgumentException(string);
        }
    }

    @Override
    public int lastIndexOf(Object o) {
        int indexForRemove = -1;
        for (int i = size -1; i >= 0; i--) {
            if (Objects.equals(o, massiv[i])) {
                indexForRemove = i;
                break;
            } else {

            }
        }
        return indexForRemove;
    }

    @Override
    public ListIterator<String> listIterator() {
        return null;
    }

    @Override
    public ListIterator<String> listIterator(int index) {
        return null;
    }

    @Override
    public List<String> subList(int fromIndex, int toIndex) {
        return null;
    }


    private static class CustomIterator implements Iterator<String> {

        private int currentIndex = -1;
        private final List<String> list;
        private boolean currentIsRemoved = false;

        public CustomIterator(List<String> list) {
            this.list = list;
        }

        @Override
        public boolean hasNext() {
            return currentIndex + 1 < list.size();
        }

        @Override
        public String next() {
            currentIndex++;
            String value = list.get(currentIndex);
            currentIsRemoved = false;
            return value;
        }

        @Override
        public void remove() {
            if (!currentIsRemoved) {
                list.remove(currentIndex);
                currentIsRemoved = true;
            }
        }
    }
}
