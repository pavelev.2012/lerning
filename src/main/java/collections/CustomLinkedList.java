package collections;

import org.w3c.dom.Node;

import java.util.*;

public class CustomLinkedList implements List<String> {
    private Noda root = new Noda(null);
    public int size = 0;

    private static class Noda {
        private String value;
        private Noda next;

        public Noda(String value) {
            this.value = value;
        }

        public Noda getNext() {
            return next;
        }

        public void setNext(Noda next) {
            this.next = next;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    @Override
    public boolean add(String string) {

        Noda lastNoda = lastNoda(root);
        Noda newNoda = newNoda();
        bind(lastNoda, newNoda);
        setValue(string, newNoda);
        incrementSize();
        return true;
    }

    public Noda lastNoda(Noda first) {
        Noda currentNode = first;
        while (currentNode.getNext() != null) {
            currentNode = currentNode.getNext();
        }
        return  currentNode;
    }

    public Noda newNoda() {
        Noda noda = new Noda(null);
        return noda;
    }

    public void bind(Noda first, Noda last) {
        first.setNext(last);
    }

    public void setValue(String value, Noda noda) {
        noda.setValue(value);
    }

    public int incrementSize() {
        return size++;
    }

    @Override
    public String get(int index) {
        if (index < 0 || index >= size){
            throw new IndexOutOfBoundsException("нет такой цифры.");
        } else {
            // do noting
        }
        Noda find = root;
        for (int i = 0; i < index; i++) {
            find = find.next;
        }
        return find.getValue();
    }


    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        Noda find = root;
        for (int i = 0; i < size; i++) {
            find = find.next;
            if (find.getValue().equals(o)){
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<String> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends String> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends String> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public String set(int index, String element) {
        return null;
    }

    @Override
    public void add(int index, String element) {

    }

    @Override
    public String remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<String> listIterator() {
        return null;
    }

    @Override
    public ListIterator<String> listIterator(int index) {
        return null;
    }

    @Override
    public List<String> subList(int fromIndex, int toIndex) {
        return null;
    }
}
