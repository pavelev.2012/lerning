package org.example;



public class StringUtils {

    public static String toUpperCase(String string) {
        if (string == null) {
            return null;
        }
        return string.toUpperCase();
    }

}