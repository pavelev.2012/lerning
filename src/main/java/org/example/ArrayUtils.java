package org.example;

import java.util.ArrayList;
import java.util.Objects;

public class ArrayUtils {

    public static void copyArray(String[] oldArray, String[] newArray) {
        System.arraycopy(oldArray, 0, newArray, 0, oldArray.length);
    }

    public static void fill(Object[] destination, Object[] source) {
        System.arraycopy(source, 0, destination, 0, destination.length);
    }

    public static void moveToRight(Object[] array, int fromIndex, int size) {
        System.arraycopy(array, fromIndex, array, fromIndex + 1, size - fromIndex);
        array[fromIndex] = null;
    }

    public static int[] findIndexes(Object[] array, Object[] elements) {
        ArrayList<Integer> indexes = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            for (Object element : elements) {
                if (Objects.equals(array[i], element)) {
                    indexes.add(i);
                    break;
                }
            }
        }
        return indexes.stream()
                .mapToInt(e -> e)
                .toArray();
    }

    public static void trim(Object[] array, int size) {
        for (int i = size; i < array.length; i++) {
            array[i] = null;
        }
    }

    public static void collapse(Object[] array, int[] forRemove) {
    }
}
