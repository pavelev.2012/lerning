package collections;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NodeTest {

    private static class Node {
        private Node next;
        private String value;

        public Node(String value) {
            this.value = value;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        public String getValue() {
            return value;
        }

        private boolean isLastNode() {
            return getNext() == null;
        }

        public Node getLast() {
            return getLastWithLoop();
        }

        private Node getLastRecursive() {
            if (getNext() == null) {
                return this;
            } else {
                return getNext().getLast();
            }
        }

        private Node getLastWithLoop() {
            Node temp = this;
            while (!temp.isLastNode()) {
                temp = temp.getNext();
            }
            return temp;
        }
    }

    @Test
    void name() {
        Node root = new Node(null);
        Node node1 = new Node("1");
        Node node2 = new Node("2");
        Node node3 = new Node("3");
        Node node4 = new Node("4");
        Node node5 = new Node("5");
        root.setNext(node1);
        node1.setNext(node2);
        node2.setNext(node3);
        node3.setNext(node4);
        node4.setNext(node5);
        assertEquals(node5.getValue(), root.getLast().getValue());
        assertEquals(node5.getValue(), node2.getLast().getValue());
        assertEquals(node5.getValue(), node5.getLast().getValue());
    }

    @Test
    void name2() {
        Node root = new Node(null);
        Node temp = root;
        for (int i = 0; i <10000; i++) {
            Node temp2 = new Node("" + i);
            temp.setNext(temp2);
            temp = temp2;
        }
        Node last = root.getLast();

    }
}
