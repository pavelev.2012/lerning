package collections;

import java.util.List;

public class CustomArrayListTest extends AbstractListTest<String> {
    private List<String> list = new CustomArrayList();

    @Override
    List<String> getList() {
        return list;
    }

    @Override
    String nextValue() {
        return "123";
    }

    @Override
    String nextValue(int i) {
        return "" + i;
    }
}
