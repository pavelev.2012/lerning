package collections;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;

public abstract class AbstractListTest<T extends String> {

    abstract List<T> getList();

    abstract T nextValue();

    abstract T nextValue(int i);

    private List<T> etalon = new ArrayList<>();

    private List<T> getEtalon() {
        return etalon;
    }

    @Test
    void addToEmpty() {
        testTemplate(getEtalon(), getList(), l -> l.add(nextValue()));
    }

    @Test
    void massAdd() {
        testTemplateVoid(getEtalon(), getList(), l -> {
            for (int i = 0; i < 1000; i++) {
                l.add(nextValue(i));
            }
        });
    }

    @Test
    void massAddSame() {
        testTemplateVoid(getEtalon(), getList(), l -> {
            for (int i = 0; i < 1000; i++) {
                l.add(nextValue());
            }
        });
    }

    @Test
    void removeIf() {
        testTemplateVoid(getEtalon(), getList(), l -> {
            for (int i = 0; i < 1000; i++) {
                l.add(nextValue());
            }
            l.removeIf(o -> Objects.equals(o, nextValue()));
        });
    }

    private void assertListEquals(List etalon, List list) {
        assertEquals(etalon.size(), list.size());
        assertEquals(etalon.isEmpty(), list.isEmpty());
        Iterator etalonIterator = etalon.iterator();
        Iterator listIterator = list.iterator();
        while (etalonIterator.hasNext()) {
            assertEquals(etalonIterator.next(), listIterator.next());
        }
        for (int i = 0; i < etalon.size(); i++) {
            int tempI = i;
            assertEquals(etalon.get(i), list.get(i), () -> "не совпадают элементы со под инедксом " + tempI);
        }
    }

    private <R> void testTemplate(List<T> etalon, List<T> list, Function<List<T>, R> function) {
        assertEquals(function.apply(etalon), function.apply(list));
        assertListEquals(etalon, list);
    }

    private void testTemplateVoid(List<T> etalon, List<T> list, Consumer<List<T>> function) {
        function.accept(etalon);
        function.accept(list);
        assertListEquals(etalon, list);
    }
}
