package collections;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CustomLinkedListTest {

    List<String> list = new CustomLinkedList();
    ArrayList<String> overList = new ArrayList<>();

    @Test
    void addAndGet() {
        list.add("123");
        list.add("1232323");
        list.add("12356");
        assertEquals("12356", list.get(2));
    }

    @Test
    void addSimple() {
        boolean result = list.add("123");
        assertTrue(result);
    }

    @Test
    void sizeForEmpty() {
        assertEquals(0, list.size());
    }

    @Test
    void sizeForFirst() {
        list.add("34345");
        assertEquals(1, list.size());
    }

    @Test
    void addDouble() {
        list.add("2323");
        list.add("23");
        assertEquals(2, list.size());
    }

    @Test
    void addFive() {
        list.add("2323");
        list.add("23");
        list.add("28");
        list.add("29");
        list.add("25464578");
        assertEquals(5, list.size());
    }

    @Test
    void addNull() {
        list.add(null);
        assertEquals(1, list.size());
    }

    @Test
    void addALotOf() {
        for (int i = 0; i < 20; i++) {
            list.add("2323");
        }
        assertEquals(20, list.size());
    }

    @Test
    void containsPositive() {
        list.add("45645");
        assertEquals(true, list.contains("45645"));
    }

    @Test
    void containsNegative() {
        list.add("privet");
        assertEquals(false, list.contains("dfgdfgd"));
    }

    @Test
    void containsTwo() {
        list.add("345");
        list.add("123");
        assertEquals(true, list.contains("123"));
    }

    @Test
    void contains3() {
        assertEquals(false, list.contains("345"));
    }

    @Test
    void containsNull() {
        list.add(null);
        assertEquals(true, list.contains(null));
    }

    @Test
    void containsEqualsButNotSame() {
        list.add("123");
        assertEquals(true, list.contains(new String("123")));
    }

    @Test
    void containsEqualsButNotSameAndNotNull() {
        list.add(null);
        assertEquals(false, list.contains(new String("123")));
    }

    @Test
    void containsEqualsButNotSameAndNotNull1() {
        list.add("123");
        assertFalse(list.contains(null));
    }

    @Test
    void toArray() {
        list.add("345");
        list.add("349");
        list.add("347");
        Object[] array = list.toArray();
        assertArrayEquals(new Object[]{"345", "349", "347"}, array);
    }

    @Test
    void toArray2() {
        list.add("345");
        list.add("349");
        list.add("347");
        assertArrayEquals(new String[]{"345", "349", "347"}, list.toArray(new String[0]));
    }

    @Test
    void remove() {
        list.add("4546");
        list.add("454");
        list.add("454678");
        assertTrue(list.remove("454"));
        assertEquals(2, list.size());
        assertFalse(list.contains("454"));
        assertArrayEquals(new String[]{"4546", "454678"}, list.toArray());
    }

    @Test
    void removeWhenfULL() {
        list.add("4546");
        list.add("454");
        list.add("454678");
        for (int i = 0; i < 7; i++) {
            list.add("i" + i);
        }
        assertTrue(list.remove("454"));
        assertEquals(9, list.size());
        assertFalse(list.contains("454"));
    }

    @Test
    void removeLastWhenfULL() {
        for (int i = 0; i < 10; i++) {
            list.add("i" + i);
        }
        assertTrue(list.remove("i9"));
        assertEquals(9, list.size());
        assertFalse(list.contains("i9"));
    }

    @Test
    void removeNotExists() {
        list.add("123");
        assertEquals(false, list.remove("456"));
        assertEquals(1, list.size());
        assertTrue(list.contains("123"));
    }

    @Test
    void indexOfPositive() {
        list.add("123");
        assertEquals(0, list.indexOf("123"));
    }

    @Test
    void indexOfNegative() {
        list.add("123");
        assertEquals(-1, list.indexOf("13"));
    }

    @Test
    void removeOne() {
        list.add("234");
        list.add("23478");
        list.add("24");
        list.add("23");
        assertEquals("24", list.remove(2));
        assertEquals(3, list.size());

    }

    @Test
    void containsAllFull() {
        ArrayList<String> list1 = new ArrayList<>();
        list.add("123");
        list.add("12304");
        list.add("12367");
        list.add("129");
        list1.add("123");
        list1.add("12304");
        list1.add("12367");
        list1.add("129");
        assertEquals(true, list.containsAll(list1));
    }

    @Test
    void containsAllNull() {
        list.add("23");
        list.add("235");
        list.add(null);
        overList.add("236");
        overList.add("231");
        overList.add("23");
        assertEquals(false, list.containsAll(overList));
    }

    @Test
    void containsAllEmpty() {
        list.add("234");
        assertEquals(true, list.containsAll(overList));
    }

    @Test
    void addAll() {
        overList.add("234");
        overList.add("234");
        overList.add("234");
        assertEquals(true, list.addAll(overList));
    }

    @Test
    void addAllEmpty() {
        assertEquals(false, list.addAll(overList));
    }

    @Test
    void addByIndexFull() {
        for (int i = 0; i < 12; i++) {
            list.add(0, "123");
        }
    }

    @Test
    void addByBadIndex() {
        assertThrows(IllegalArgumentException.class, () -> list.add(-1, "123"));
    }

    @Test
    void set() {
        list.add("123");
        list.add("13");
        list.add("19993");
        assertEquals("13", list.set(1, "456"));
        assertEquals(3, list.size());
    }

    @Test
    void setByBadIndex() {
        assertThrows(IllegalArgumentException.class, () -> list.set(-1, "123"));
    }

    @Test
    void clearMetods() {
        list.add("1235");
        list.add("1235324");
        list.add("1234545");
        list.add("123");
        list.clear();
        assertEquals(-1, list.indexOf("1235"));
        assertEquals(0, list.size());
    }

    @Test
    void lastIndex() {
        list.add("123456");
        list.add("123456");
        list.add("12456");
        list.add("12356");
        list.add("1234");
        assertEquals(0, list.indexOf("123456"));
        assertEquals(1, list.lastIndexOf("123456"));
        assertEquals(-1, list.lastIndexOf("12"));
    }

    @Test
    void lastIndexSameSize() {
        for (int i = 0; i < 15; i++) {
            list.add("123");
        }
        assertEquals(14, list.lastIndexOf("123"));
    }

    @Test
    void iterator() {
        List<String> expected = List.of("123", "456", "789");
        list.addAll(expected);

        List<String> result = new ArrayList<>();
        list.forEach(result::add);

        assertEquals(expected, result);
    }

    @Test
    void iteratorRemove() {
        List<String> expected = List.of("123", "456", "789");
        list.addAll(expected);

        Iterator<String> iterator = list.iterator();
        iterator.next();
        iterator.next();
        iterator.remove();

        assertEquals(List.of("123", "789"), list);
    }
}