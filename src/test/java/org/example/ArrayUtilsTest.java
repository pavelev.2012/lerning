package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayUtilsTest {

    @Test
    void first() {
        String[] array = {"123", "456", null, null, null};
        ArrayUtils.moveToRight(array, 1, 2);
        Assertions.assertArrayEquals(new String[]{"123", null, "456", null, null}, array);
    }
    @Test
    void Two() {
        String[] array = {"123", "456", null, null, null};
        ArrayUtils.moveToRight(array, 0,2);
        Assertions.assertArrayEquals(new String[]{null, "123", "456", null, null}, array);
    }

    @Test
    void arrayUtils() {
        String[] array = {"123", "456", "789", "057"};
        String[] elemets = {"12345", "45623", "789", "057"};
        int[] indexs = ArrayUtils.findIndexes(array, elemets);
        assertArrayEquals(new int[]{2,3}, indexs);
    }
}