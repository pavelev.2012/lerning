package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class StringUtilsTest {

    @Test
    public void toUpperCaseForEN() {
        assertEquals("DFDSJGS;LD", StringUtils.toUpperCase("dfdsjgs;ld"), "код говно");
    }

    @Test
    public void toUpperCaseForRU() {
        assertEquals("ВЫЛОПРВЫАЛДП", StringUtils.toUpperCase("вылопрвыалдп"), "код говно");
    }

    @Test
    public void toUpperCaseForNull() {
        assertEquals(null, StringUtils.toUpperCase(null), "код говно");
    }
    @Test
    public void toUpperCaseForEmpty() {
        assertEquals("", StringUtils.toUpperCase(""), "код говно");
    }
}